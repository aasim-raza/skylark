#!/bin/bash

# ensure that we are running as user nobody
if [ `whoami` != root ]
then
  echo Please run this script as user root.
  exit 2
fi

if [ $# -ne 1 ]
then
    echo "Error in $0 - Invalid Argument Count"
    echo "Syntax: $0 user|admin|merchant"
    exit
fi

type=$1

# webroot is auto created when apache is installed
htmlroot=/var/www/html/hotelmanagement
webroot=$htmlroot/$type
batchroot=$htmlroot/batches/$type

#copy the htaccess file for udb - for various security related restrictions
cp ../php/udb/.htaccess $htmlroot/udb/
cp ../php/udb/.htaccess $htmlroot/udb/$type/

cp ../php/udb/.htaccess $htmlroot/bin/

# Copy the dir structure
cp -r ../php/commoncore $htmlroot/
cp -r ../php/htmlpurifier $htmlroot/
cp -r ../php/$type/* $webroot/

#copy htaccess
#cp -f ../php/$type/.htaccess $webroot/

#copy batch files
cp -r ../php/batches/$type/* $batchroot/

#copy config_production.phi to config.phi
curr_host=`hostname`
echo $curr_host | grep 'prod'  1>/dev/null 
if [ `echo $?` -eq 0 ]
then
cp ../php/commoncore/SERVER_CONFIG-production.phi $htmlroot/commoncore/SERVER_CONFIG.phi
fi

#remove all .svn files
find $htmlroot -name .svn -exec rm -rf {} \; 2>/dev/null

chown apache:apache -R $batchroot/logs
chown apache:apache -R $htmlroot/htmlpurifier/library/HTMLPurifier/DefinitionCache/Serializer

# display the content of webroot
echo Webroot $webroot contents
ls -al $webroot/

#display content of batch folder
echo batch folders $batchroot contents
ls -al $batchroot/

#restart the apache
echo "###############################################"
echo #
echo restarting the apache now please wait....
service httpd restart
echo #
echo "###############################################"