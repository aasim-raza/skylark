#!/bin/bash

# ensure that we are running as user nobody
if [ `whoami` != root ]
then
  echo Please run this script as user root.
  exit 2
fi

if [ $# -ne 1 ]
then
    echo "Error in $0 - Invalid Argument Count"
    echo "Syntax: $0 user|admin|merchant"
    exit
fi

type=$1

# webroot is auto created when apache is installed
htmlroot=/var/www/html/hotelmanagement
webroot=$htmlroot/$type
batchroot=$htmlroot/batches/$type

mkdir -p $webroot
mkdir -p $htmlroot/bin
mkdir -p $batchroot/logs
mkdir -p $htmlroot/udb/$type
chown apache:apache -R $batchroot/logs
chown apache:apache -R $htmlroot/udb

