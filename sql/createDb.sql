CREATE DATABASE hotelsystem;
CREATE USER hoteluser IDENTIFIED BY 'hotelpass';

GRANT ALL ON hotelsystem.* to hoteluser@'%' identified by 'hotelpass';
GRANT ALL ON hotelsystem.* to hoteluser@'localhost' identified by 'hotelpass';
FLUSH PRIVILEGES;