
ALTER TABLE `booking_rec` CHANGE `checkout_time` `checkout_time` TIME NOT NULL DEFAULT '00:00:00';
ALTER TABLE `booking_rec` CHANGE `nationality` `nationality` VARCHAR(16) NOT NULL;


-- 24 01 2018  
-- Aasim
ALTER TABLE `user_rec` ADD `mobile` VARCHAR(32) NOT NULL AFTER `name`, ADD `email` VARCHAR(256) NOT NULL AFTER `mobile`, ADD `address` VARCHAR(512) NOT NULL AFTER `email`, ADD `is_password_change` CHAR(1) NOT NULL DEFAULT 'N' AFTER `address`;
ALTER TABLE `booking_rec` DROP `number_of_nights`;
ALTER TABLE `booking_rec` ADD `week_days` INT(10) NOT NULL AFTER `room_type`, ADD `weekend_days` INT(10) NOT NULL AFTER `week_days`;