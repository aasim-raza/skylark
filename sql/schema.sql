CREATE TABLE hotel_master_rec 
(
    hotel_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    hotel_id VARCHAR(512) NOT NULL , 
    hotel_name VARCHAR(1024) NOT NULL , 
    address TEXT NOT NULL , 
    mobile_no VARCHAR(11) NOT NULL , 
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_hotel_seq(hotel_seq),
    UNIQUE uk_hotel_seq (hotel_seq),
    INDEX in_hotel_seq(hotel_seq)
) ENGINE = InnoDB;


CREATE TABLE user_rec 
(
    hotel_seq INT(11) UNSIGNED NOT NULL,
    user_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    user_name VARCHAR(512) NOT NULL,
    password VARCHAR(512) NOT NULL,
    role CHAR(2) NOT NULL,
    status CHAR(2) NOT NULL DEFAULT 'A',
    name VARCHAR(128) NOT NULL,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_user_seq(user_seq),
    UNIQUE uk_user_name (user_name),
    INDEX in_hotel_seq(hotel_seq)
) ENGINE = InnoDB;


CREATE TABLE hotel_detail_rec (

    hotel_seq int(11) NOT NULL,
    from_email varchar(256) NOT NULL,
    return_email varchar(256) NOT NULL,
    sms_user varchar(256) NOT NULL,
    sms_password varchar(256) NOT NULL
) ENGINE=InnoDB;


CREATE TABLE booking_rec 
( 
    hotel_seq INT(11) UNSIGNED NOT NULL,
    booking_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    reservation_no VARCHAR(64) NOT NULL,
    fname VARCHAR(256) NOT NULL,
    lname VARCHAR(256) NOT NULL,
    checkin_date DATE NOT NULL,
    checkin_time TIME NOT NULL,
    checkout_date DATE NOT NULL,
    checkout_time TIME NOT NULL,
    number_of_nights INT(8) UNSIGNED NOT NULL,
    room_type INT(11) UNSIGNED NOT NULL,
    mobile VARCHAR(11) NOT NULL,
    email_id VARCHAR(64) NOT NULL,
    number_of_rooms INT(11) UNSIGNED NOT NULL,
    nationality INT(8) NOT NULL,
    city VARCHAR(256) NOT NULL,
    number_of_adults INT(8) NOT NULL,
    number_of_childs INT(8) NOT NULL,
    rate_plan INT(11) NOT NULL,
    total_price DOUBLE(10, 6) NOT NULL,
    status CHAR(2) NOT NULL DEFAULT 'A',
    notify_user CHAR(1) NOT NULL DEFAULT 'N',
    notify_admin CHAR(1) NOT NULL DEFAULT 'N',
    notify_site_admin CHAR(1) NOT NULL DEFAULT 'N',
    comment TEXT NOT NULL,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY pr_booking_seq(booking_seq), 
    INDEX in_reservation_no(reservation_no), 
    UNIQUE uk_reservation_no(reservation_no)
) ENGINE=InnoDB;


CREATE TABLE room_rec 
( 
    room_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    hotel_seq INT(11) UNSIGNED NOT NULL ,
    room_type INT(11) NOT NULL ,
    is_breakfast CHAR(1) NOT NULL DEFAULT 'N' ,
    room_label INT(11) NOT NULL ,
    free_cancellation CHAR(1) NOT NULL DEFAULT 'N' ,
    room_name VARCHAR(512) NOT NULL ,
    combine_occupant CHAR(1) NOT NULL DEFAULT 'N' ,
    room_desc TEXT NOT NULL ,
    pay_later CHAR(1) NOT NULL DEFAULT 'N' ,
    no_of_rooms INT(11) UNSIGNED NOT NULL ,
    weekday_price DOUBLE(10, 6) NOT NULL ,
    weekend_price DOUBLE(10, 6) NOT NULL ,
    room_size VARCHAR(256) NOT NULL ,
    bed_type VARCHAR(512) NOT NULL ,
    show_daily_price CHAR(1) NOT NULL DEFAULT 'N' ,
    is_enable CHAR(1) NOT NULL DEFAULT 'N',
    images TEXT NOT NULL ,
    facilities TEXT NOT NULL ,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_room_seq (room_seq),
    INDEX in_room_seq(room_seq)
)ENGINE = InnoDB;


CREATE TABLE room_type_rec 
(   
    hotel_seq int(11) NOT NULL,
    room_type_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    room_type VARCHAR(512) NOT NULL ,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_room_type_seq(`room_type_seq`)
) ENGINE = InnoDB;


CREATE TABLE room_label_rec 
(   
    hotel_seq int(11) NOT NULL,
    room_label_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    room_label VARCHAR(512) NOT NULL ,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_room_label_seq(`room_label_seq`)
) ENGINE = InnoDB;



CREATE TABLE bill_rec 
(   
    bill_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    booking_seq int(11) NOT NULL,
    service_seq int(11) NOT NULL,
    amount FLOAT(11) NOT NULL,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_bill_seq(bill_seq),
    INDEX in_booking_seq(booking_seq)
) ENGINE = InnoDB;


CREATE TABLE services_charge_rec 
(   
    service_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    service_name VARCHAR(512) NOT NULL ,
    tax FLOAT(11) NOT NULL ,
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_service_seq(service_seq),
    INDEX in_service_seq(service_seq)
) ENGINE = InnoDB;


CREATE TABLE payment_rec 
(   
    payment_seq INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
    booking_seq int(11) NOT NULL,
    amount FLOAT(11) NOT NULL,
    payment_info TEXT NOT NULL ,
    payment_type CHAR(2) NOT NULL DEFAULT 'C', -- C -Cash O -Online CH - cheaque
    status CHAR(2) NOT NULL DEFAULT 'N', -- N -Not R- Received
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    update_time TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    PRIMARY KEY pk_payment_seq(payment_seq),
    UNIQUE uk_booking_seq (booking_seq),
    INDEX in_booking_seq(booking_seq)
) ENGINE = InnoDB;


-- INSERT INTO `room_type_rec` (`hotel_seq`, `room_type`) VALUES
-- (1, 'Hotel Room'),
-- (1, 'Studio'),
-- (1, 'Hostel (Dorm Room)'),
-- (1, 'Apartment'),
-- (1, 'Guest House'),
-- (1, 'Bed Breakfast'),
-- (1, 'Villa');