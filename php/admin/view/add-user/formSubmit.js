_userSeq = 0;
$(document).ready(function(){

/*	var roomSeq = getUrlParameter('edit');
	if(!empty(roomSeq)){

		_roomSeq = roomSeq;
		getEditInterface(roomSeq);
		isEdit = true;	
	}*/


});




$(document).on('click', '.close', function(e){

	e.preventDefault();
	var url = $(this).attr('url');
	
	removeImage(url);
	$(this).parent('.thumbnail').parent('div').remove();
});

$(document).on('submit', '#idFormAddUser', function(e){

	e.preventDefault();

	if($(this).valid()){

		var userRole = $('#idUserRole :selected').val();
		var name = $('input[name="user_name"]').val();
		var mobileNumber = $('input[name="mobile_number"]').val();
		var email = $('input[name="email"]').val();
		var address = $('input[name="address"]').val();
		

		if(empty(userRole)){

			showAlertMessage('error', 'Error', 'Please select any user role');
			return false;
		}

	
		if(isValidEmail(email) == false){

			showAlertMessage('error', 'Error', 'Please provide valid email id');
			return false;
		}		
			
		var formData = {

			user_role: userRole,
			name: name,
			mobile_number: mobileNumber,
			email: email,
			address: address
		};

		formData = JSON.stringify(formData);
		//blockUI();

		$.post('/admin/services/processRequest',
			{
				cmd: 'addUser',
				data: formData,
				user_seq: _userSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				
				if(errorCode == 0){
					
					showAlertMessage('success', 'Success', 'User created successfully');
					//setTimeout(function(){ window.top.location = '/admin/rooms' }, 4000);

				}else if(errorCode){

					showAlertMessage('error', 'Error', data['errorMsg']);
				}

				unblockUI();
			}

		)
	}
	
});


/*function getEditInterface(roomSeq){

	if(!empty(roomSeq)){

		blockUI();
		$.post('/admin/services/processRequest',
			{
				cmd: 'getRoomInfo',
				seq: roomSeq,
				hotel_seq: hotelSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				var row = data['data'][0];
				
				if(!empty(row)){

					$('#idRoomType').val(row['room_type_seq']);
					$('#idRoomLabel').val(row['room_label']);
					$('#idBreakfast').val(row['is_breakfast']);

					if(row['free_cancellation'] == 'Y'){
						
						$('input[name="free_cancellation"]').prop('checked', true);
					}

					if(row['combine_occupant'] == 'Y'){
						
						$('input[name="combine_occupant"]').prop('checked', true);
					}

					if(row['pay_later'] == 'Y'){
						
						$('input[name="pay_later"]').prop('checked', true);
					}

					if(row['show_daily_price'] == 'Y'){
						
						$('input[name="daily_price"]').prop('checked', true);
					}

					var arrFacilities = $.parseJSON(row['facilities']);
					if(!empty(arrFacilities)){

						$.each(arrFacilities, function(key, val){

							$('input[name="'+val+'"]').prop('checked', true);
						});
					}

					var arrHotelImages = $.parseJSON(row['images']);
					if(!empty(arrHotelImages)){

						$.each(arrHotelImages, function(key, val){

							setHotelImages(val);
							hotelImages.push(val);
						});
					}

					$('input[name="room_name"]').val(row['room_name']);
					$('input[name="no_of_rooms"]').val(row['no_of_rooms']);
					$('input[name="weekday_price"]').val(parseFloat(row['weekday_price']));
					$('input[name="weekend_price"]').val(parseFloat(row['weekend_price']));
					$('input[name="room_size"]').val(row['room_size']);
					$('input[name="bed_type"]').val(row['bed_type']);
					$('#idTextRoomDescription').val(row['room_desc']);
				}

				unblockUI();
			}
		)
	}
}*/