var bookingSeq = 0;
var addressImages = [];

var myDropzone;
var totalImages = 1;
var isRoomAvailable = 0;

$(document).ready(function(){

	$('.pickatime').pickatime();
	$('#idFormCreateBooking').validate();

	bookingSeq = getUrlParameter('edit');
	if(!empty(bookingSeq)){
		getBookingEditInterface(bookingSeq);
		isEdit = true;	
	}

	Dropzone.autoDiscover = false; 
	initializeDropZone();
});

var userSeq;
var date = new Date(); 
var maxDate;

$(function(){
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: date,
		onSelect: function(dateText, inst){

			$("#idCheckOutDate").datepicker("option", "minDate", dateText);
		}
	});
});

$(function(){
	$('#idCheckOutDate').datepicker({
		dateFormat: "yy-mm-dd"
	});
});

$(document).on('submit', '#idFormCreateBooking', function(e){

	e.preventDefault();

	if($(this).valid()){

		var firstName = $('input[name="first_name"]').val();
		var lastName = $('input[name="last_name"]').val();
		var email = $('input[name="email"]').val();
		var mobile = $('input[name="mobile"]').val();

		var checkInDate = $('input[name="check_in_date"]').val();
		var checkInTime = $('input[name="check_in_time"]').val();
		var checkOutDate = $('input[name="check_out_date"]').val();
		var country = $('#idSelectCountry :selected').val();
		var city = $('input[name="city"]').val();

		var numberOfRooms = $('input[name="number_of_rooms"]').val();
		var roomType = $('#idRoomType :selected').val();
		var numberOfAdult = $('#idNumberOfAdults').val();
		var numberOfChilds = $('#idNumberOfChilds').val();

		// var numberOfAdult = $('input[name="number_of_adult"]').val();
		// var numberOfChilds = $('input[name="number_of_childs"]').val();

		var reatePlan = $('#idRatePlan :selected').val();
		var totalAmount = $('input[name="total_amount"]').val();
		var comment = $('#idTextSuggetion').val();
		var notifyUser = $('input[name="notifiy_user"]:checked').length;
		var notifyHotelAdmin = $('input[name="notifiy_hotel_admin"]:checked').length;
		var notifySiteAdmin = $('input[name="notifiy_site_admin"]:checked').length;

		if(isValidEmail(email) == false){

			showAlertMessage('error', 'Error', 'Please provide valid email id');
			return false;
		}

		if(empty(country)){

			showAlertMessage('error', 'Error', 'Please select any country');
			return false;
		}

		if(empty(roomType)){
			
			showAlertMessage('error', 'Error', 'Please select any room type');
			return false;
		}

		if(empty(addressImages)){
			
			showAlertMessage('error', 'Error', 'Please upload any address proof');
			return false;
		}

		var strAddressImage = JSON.stringify(addressImages);
		var formData = {

			fname: firstName,
			lname: lastName,
			email_id: email,
			mobile: mobile,
			checkin_date: checkInDate,
			checkin_time: checkInTime,
			checkout_date: checkOutDate,
			nationality: country,
			city: city,
			number_of_rooms: numberOfRooms,
			room_type: roomType,
			number_of_adults: numberOfAdult,
			number_of_childs: numberOfChilds,
			rate_plan: reatePlan,
			total_price: totalAmount,
			comment: comment,
			notify_user: notifyUser,
			notify_admin: notifyHotelAdmin,
			notify_site_admin: notifySiteAdmin,
			address_proof: strAddressImage
		};

		blockUI();
		formData = JSON.stringify(formData);
		$.post('/admin/services/processRequest',
			{
				cmd: 'addBooking',
				data: formData,
				booking_seq: bookingSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				
				if(errorCode == 0){
					
					showAlertMessage('success', 'Success', data['data']);
					setTimeout(function(){ window.top.location = '/admin/booking' }, 4000);

				}else if(errorCode){

					showAlertMessage('error', 'Error', data['errorMsg']);
				}

				unblockUI();
			}
		)
	}
	
});

function getBookingEditInterface(bookingSeq){

	if(!empty(bookingSeq)){

		blockUI();
		$.post('/admin/services/processRequest',
			{
				cmd: 'getSearchResult',
				booking_seq: bookingSeq,
				formatted_response: 1

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				
				var dataRow = data['data'][0];
				if(errorCode == 0){
					
					if(!empty(dataRow) && dataRow['orig_status'] == 'A'){
						
						$('input[name="first_name"]').val(dataRow['fname']);
						$('input[name="last_name"]').val(dataRow['lname']);
						$('input[name="email"]').val(dataRow['email_id']);
						$('input[name="mobile"]').val(dataRow['mobile']);
						$('input[name="check_in_date"]').val(dataRow['checkin_date_orig']);
						$('input[name="check_in_time"]').val(dataRow['checkin_time']);
						$('input[name="check_out_date"]').val(dataRow['checkout_date']);
						$('#idSelectCountry').val(dataRow['nationality']);
						$('input[name="city"]').val(dataRow['city']);

						$('input[name="number_of_rooms"]').val(dataRow['number_of_rooms']);
						$('#idRoomType').val(dataRow['room_type_seq']);

						setRoomCapacity(dataRow['total_adults'], dataRow['total_childs']);
						$('#idNumberOfAdults').val(dataRow['number_of_adults']);
						$('#idNumberOfChilds').val(dataRow['number_of_childs']);

						// $('input[name="number_of_adult"]').val(dataRow['number_of_adults']);
						// $('input[name="number_of_childs"]').val(dataRow['number_of_childs']);
						$('#idRatePlan').val(dataRow['rate_plan']);
						$('input[name="total_amount"]').val(parseFloat(dataRow['total_price']));
						$('#idTextSuggetion').val(dataRow['comment']);


						if(dataRow['notify_user'] == 'Y'){

							$('input[name="notifiy_user"]').prop('checked', true);
							$('input[name="notifiy_user"]').parent('span').addClass('checked');
						}

						if(dataRow['notify_admin'] == 'Y'){

							$('input[name="notifiy_hotel_admin"]').prop('checked', true);
							$('input[name="notifiy_hotel_admin"]').parent('span').addClass('checked');
						}

						if(dataRow['notify_site_admin'] == 'Y'){

							$('input[name="notifiy_site_admin"]').prop('checked', true);
							$('input[name="notifiy_site_admin"]').parent('span').addClass('checked');
						}

						if(!empty(dataRow['address_proof'])){

							addressImages = $.parseJSON(dataRow['address_proof']);
							setImagesOnDropzone();
						}
						
					}else{

						window.location.href = '/admin/booking';
					}
				}

				unblockUI();
			}
		)
	}
}

$(document).on('click', '#idLabelCheckAvailablity', function(e){

	var checkInDate = $('input[name="check_in_date"]').val();
	var checkOutDate = $('input[name="check_out_date"]').val();

	var numberOfRooms = $('input[name="number_of_rooms"]').val();
	var roomType = $('#idRoomType :selected').val();

	var colorClass = 'colorGreen';
	$('#idLabelCheckAvailablityMsg').removeClass(colorClass);
	$('input[name="total_amount"]').val('');

	if(empty(checkInDate)){

		showAlertMessage('error', 'Error', 'Please select check in date');
		return false;
	}

	if(empty(checkOutDate)){

		showAlertMessage('error', 'Error', 'Please select check out date');
		return false;
	}

	if(empty(roomType)){

		showAlertMessage('error', 'Error', 'Please select room type');
		return false;
	}

	if(empty(numberOfRooms)){

		showAlertMessage('error', 'Error', 'Please select atleast one room for booking');
		return false;
	}

	
	blockUI();
	$.post('/admin/services/processRequest',
		{
			cmd: 'checkAvailablity',
			check_indate: checkInDate,
			check_outdate: checkOutDate,
			room_type: roomType,
			number_of_rooms: numberOfRooms,
			booking_seq: bookingSeq

		},function(response){

			var data = $.parseJSON(response);

			var errorCode = data['errorCode'];
			var message = 'Room is available';
			
			if(errorCode == 0){
				
				var info = data['data'];
				message = info['msg'];

				colorClass = info['class'];
				$('input[name="total_amount"]').val(info['amount']);

			}else if(errorCode == 1){

				showAlertMessage('error', 'Error', data['errorMsg']);

			}

			$('#idLabelCheckAvailablityMsg').addClass(colorClass);
			$('#idLabelCheckAvailablityMsg').html(message);

			unblockUI();
		}
	)
});

$(document).on('change', '.classRoomCapacity', function(e){

	var roomSeq = $('#idRoomType :selected').val();
	var numberOfRooms = $('input[name="number_of_rooms"]').val();

	if(!empty(roomSeq) && !empty(numberOfRooms)){
		
		$.post('/admin/services/processRequest',
			{
				cmd: 'getRoomCapacity',
				room_seq: roomSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];

				var strHtml = '';
				data = data['data'];
				
				if(errorCode == 0 && !empty(numberOfRooms)){
					
					setRoomCapacity((data['number_of_adults'] * numberOfRooms), (data['number_of_childs'] * numberOfRooms));
				}
			}
		)
	}
})

function setRoomCapacity(adults, childs){

	// strHtml = '<option value="0">Select Any One</option>';
	var strHtml = '';
	for(var x = 1; x <= adults; ++x){

		strHtml += '<option value="'+x+'">'+x+'</option>';

	}
	$('#idNumberOfAdults').html(strHtml);

	// strHtml = '<option value="0">Select Any One</option>';
	strHtml = '';
	for(var x = 0; x <= childs; ++x){

		strHtml += '<option value="'+x+'">'+x+'</option>';

	}
	$('#idNumberOfChilds').html(strHtml);
}

function initializeDropZone(){

	var fileOptions = {}; 
    fileOptions[0] = ['main', '800','600','90'];
   
	myDropzone = new Dropzone("#dropzone_file_limits", 
					{ 
						url: "/admin/services/processRequest",
						headers: { "cmd": "uploadImage", "params": JSON.stringify(fileOptions), "folder": "address"},
						paramName: "file", // The name that will be used to transfer the file
				        dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
				        maxFilesize: 0.4, // MB
				        maxFiles: 1,
				        maxThumbnailFilesize: 1,
				        acceptedFiles : "image/png, image/jpeg, image/bmp",
				        addRemoveLinks: true,
				        success: function(file, serverResponse) {


							var data = $.parseJSON(serverResponse);
							var url = '';
							var fileName = file['name'];

							if(data['errorCode'] == 0 && !empty(data['data'])){

								var imageData = data['data'];
								url = imageData[fileName]['main'];

								addressImages.push(url);
							}
					    }
					}
				);
}

function setImagesOnDropzone(){

	if(!empty(addressImages)){

		var mockFile;
		var name; 

		var size;
		var index;

		$.each(addressImages, function(key, val){

			index = val.indexOf('main_');
			//name = val.substr(index, 13);

			mockFile = { name: val, size: 1234};
			myDropzone.options.addedfile.call(myDropzone, mockFile);
			myDropzone.options.thumbnail.call(myDropzone, mockFile, val);
		});
	}
 

	var existingFileCount = addressImages.length; // The number of files already uploaded
	myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;

	myDropzone.on('removedfile', function(file){
		
		//remove images from array
		var index = addressImages.indexOf(file['name']);
		addressImages.splice(index, 1);

		var existingFileCount = addressImages.length; // The number of files already uploaded			
		myDropzone.options.maxFiles = (totalImages - existingFileCount);
	})
}

function getRommNumber(selectedRoomNo){
	
	var checkInDate = $('input[name="check_in_date"]').val();
	var checkOutDate = $('input[name="check_out_date"]').val();

	var numberOfRooms = $('input[name="number_of_rooms"]').val();
	var roomType = $('#idRoomType :selected').val();

	$.post('/admin/services/processRequest',
		{
			cmd: 'getRoomNumbers',
			check_indate: checkInDate,
			check_outdate: checkOutDate,
			room_type: roomType,
			booking_seq: bookingSeq

		},function(response){

			var data = $.parseJSON(response);
			var errorCode = data['errorCode'];
			
			if(errorCode == 0){
				
				var info = data['data'];
				addRoomNumber(info, selectedRoomNo)				

			}
			unblockUI();
		}
	)
}

function addRoomNumber(arrRoomNumber, selectedRoomNo){

	var strHtml = '<option value=0>Select Any One</option>';
	if(!empty(arrRoomNumber)){

		$.each(arrRoomNumber, function(key, val){

			strHtml += '<option value="'+val+'">'+val+'</option>';
		});
	}

	$('#idRoomNumber').html(strHtml);
	if(!empty(selectedRoomNo)){

		$('#idRoomNumber').val(selectedRoomNo);
	}
}