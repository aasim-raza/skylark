<?php
include_once '../../commoncore/config.phi';
include_once '../../commoncore/context.phi';
include_once '../core/login.phi';
include_once '../../commoncore/coreFunctions.phi';
include_once '../../commoncore/displayFunctions.phi';
 

/**
 * Figure out the basename of the script, which can
 * be reused by the modules which are loaded down.
 */
$page = $_REQUEST['page'];
$who  = $_REQUEST['who'];
set_context('PAGE', $page);
set_context('WHO', $who);

unset($GLOBALS['error_page_msg']);

$action = '';
if($page == 'info'){

    $action = $_REQUEST['action'];
    $pageInfo = getPageFetailInfo($action);

    set_context('TITLE', $pageInfo['title']);
    set_context('HEADING', $pageInfo['heading']);
}

/**
 * Even this should not be here. Every page that needs to
 * check if the user is logged in should do this check there.
 */

if($page == ''){

  doRedirect('/');
  exit;
}

 
/*if($page != 'a_login' && !userLoggedIn1())
{
  destroyCookies1();
  $doneUri = urlencode($_SERVER['REQUEST_URI']);
  doRedirect("/my/a_login?done=$doneUri");
}*/

if(strpos($page, '.') !== false) //these are the cases when files requests leak in
{
  header("HTTP/1.0 404 Not Found");
  exit;
}

 
$open_pages = array('front_page', 'login');
//dump_context();
if(!userLoggedIn() && !in_array($page, $open_pages))
{
  $url = urlencode(getCurrentUrl('who'));
  doRedirect("/{$who}/login?msg=1&action=login&done={$url}");
  exit;
}
 
/*
if($page != 'pg' && !in_array($page, $open_pages) && getOrderCheckVarVal())
{
  doRedirect('/my/pg');
}
*/

if(userLoggedIn()){

    $isPasswordChange = AdminService::getUserInfo('is_password_change');
    if($isPasswordChange == 'N' && $page != 'change-password'){

        doRedirect("/{$who}/change-password");  
    }
}

$basenames = array();
ob_start();
$bver = '.phi';

/**
 * Everything that needs to be done while the
 * HTTP headers being spit out for the client.
 */
include_once "./$page/index.phi";

if(isset($includes))
{
  foreach ($includes as $include)
  {
    $basename = substr($include, 0, -4);
    include_once "$basename.phi"; //make sure to include only .phi files
    array_push($basenames, basename($basename));
  }
}
else
{
  $GLOBALS['error_page_msg'] = "<b>Requested page does not exist on our site. If you think this is an error, please <a href=/my/contact>contact us</a></b>. <br> <br>Please continue browsing at <a href=/>".DOMAIN_BASE_NAME."</a>";
}
ob_clean();

if(in_context('SHOW_LOOSE_DOCTYPE'))
{
  echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
}
elseif(in_context('SHOW_STRICT_DOCTYPE'))
{
  echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">';
}
else
{
  echo '<!DOCTYPE html>';
}

if(in_context('HEAD_PREFIX'))
{
  $headPrefix = ' prefix="'.get_context('HEAD_PREFIX').'" ';
}
else
{
  $headPrefix = '';
}

?>

<html lang="en-US">
<head <?php echo $headPrefix; ?> >
<link rel="icon" href="/images/<?php echo DOMAIN_ID; ?>/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/images/<?php echo DOMAIN_ID; ?>/favicon.ico" type="image/x-icon" />
 
<?php
/**
 * Modules generate the html which goes between the <head> and
 * the </head> tag of the page, usually CSS, meta tags. These
 * calls should not put Description and Keywords meta-tags but
 * instead should populate the "keywords" and "description" arrays
 * in the common context. The data from these vectors are collected
 * at the end and put before the rest of the tags are rendered.
 */
if(isset($includes)) {
  foreach ($basenames as $basename) {
    $funcname = $basename . '_head';
    $funcname(get_context_hash());
  }
}


/**
 * During head calls if there was any error detected,
 * stop rendering the current page, and redirect the
 * user to the error page.
 */
if (isset($GLOBALS['error_page_msg'])) {
  ob_end_clean();

  if(isset($_REQUEST['_done'])) {
    $done = $_SERVER['REQUEST_URI'] . '&.done=' . urlencode($_REQUEST['_done']);
  }
  else {
    $done = $_SERVER['REQUEST_URI'];
  }

  //doRedirect('/my/error?.done=' . urlencode($done) . '&err_msg=' . urlencode($GLOBALS['error_page_msg']));
  doRedirect('/');
  return;
}

ob_end_flush();

/**
 * Before the </head> is put, make sure that we have put the
 * SEO tags. This is important for better search ranking.
 */

if(in_context('ROBOTS_NO_INDEX'))
{
  echo '<meta name="robots" content="noindex">
';
}
?>
<meta charset="UTF-8"> 
<title><?php echo get_context('title'); ?></title>
<meta name="keywords" content="<?php echo cleanDoubleQuote(get_context('keywords')); ?>" />
<meta name="description" content="<?php echo cleanDoubleQuote(get_context('description')); ?>" />
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta name="author" content="Steelcoders" />
<?php
if(in_context('fb_property'))
  echo get_context('fb_property');
?>

<?php
if(!($who == 'fb' && ($page == 'homepage' || $page == 'home')))
{
?>
<script>
  if(window.top.location != window.location)
  {
    window.top.location = window.location;
  }
</script>
<?php
}

echo_included_css_code(); //echo any css code for header

?>
</head>
<?php
 flush(); //flush so that browser atleast will start fetching the CSS files

 //required classes for login page.
 $loginCss = '';
 if($page == 'login'){
    $loginCss = 'class="login-container login-cover"';
 }
?>
<body <?php echo $loginCss; ?>>
<?php
/**
 * Modules generate the html which goes between the <body> and
 * </body> tag of the page, usually displayed content
 */
foreach ($basenames as $basename) {
  $funcname = $basename . '_body';
  $funcname(get_context_hash());
}

echo "\n</body>\n";
/**
 * Modules generate the html which goes after the </body> tag
 * of the page, usually these are javascript codes.
 */
//echoConfigVarForJs();
//include_jq();
//include_js('/view/common/jquery.blockUI.js');
//include_js('/view/common/jquery.Jcrop.js');
//include_js('/view/common/common.js');
foreach ($basenames as $basename) {
  $funcname = $basename . '_tail';
  $funcname(get_context_hash());
}
//include_js('/view/common/popupBox.js');
//include_jq();
//include_fb();
//include_js("http://www.google.com/recaptcha/api/js/recaptcha_ajax.js");
//echo_included_js_code();
echo '</html>';
?>