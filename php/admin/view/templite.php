<?php
include_once '../../commoncore/config.phi';
include_once '../../commoncore/context.phi';
 
 
/**
 * Figure out the basename of the script, which can
 * be reused by the modules which are loaded down.
 */
$page = $_REQUEST['page'];
$include = $_REQUEST['include'];
$who  = $_REQUEST['who'];

set_context('PAGE', $page);
set_context('INCLUDE', $include);
set_context('WHO', $who);


$bver = '.phi';

$rc = 0;

include_once "./security/security.phi";
if (file_exists("./security/$page/$include$bver")) {
  include_once "./security/$page/$include$bver";
}
elseif (file_exists("./security/$page/$include.phi")) {
  include_once "./security/$page/$include.phi";
}

// Security Check callback function
if (isset($SEC_CHK) && is_callable($SEC_CHK)) {
  $rc = $SEC_CHK();
} else {
  alert_security_check_pending();
}

if ($rc) {
  if (file_exists("./$page/$include$bver")) {
    include_once "./$page/$include$bver";
  }
  else {
    include_once "./$page/$include.phi";
  }
}

?>