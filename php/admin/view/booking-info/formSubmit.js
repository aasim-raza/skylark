$(document).ready(function(){
	$('.pickatime').pickatime();
});

var userSeq;
var date = new Date();

$(function(){
	$("#datepicker").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: date,
		onSelect: function(){
			getUserAvailablity()
		}
	});
});

$(function(){
	$("#dateTestPicker").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: date
	});
});

$(document).on('change', 'input[name="check_aasign_enq"]', function(){

	userSeq = $(this).val();
	getUserAvailablity();
});

function getUserAvailablity(){
	
	var date = $("#datepicker").val();
	if(empty(date)){

		showAlertMessage('error', 'Error', 'Please enter date');
		return false;
	}

	if(empty(userSeq)){

		showAlertMessage('error', 'Error', 'Please select any user');
		return false;
	}

	$.post('/user/services/processRequest',
		{
			cmd: 'getUserAvailablity',
			date: date,
			user_seq: userSeq
		},function(response){

			var data = $.parseJSON(response);
			var errorCode = data['errorCode'];

			var strHtml = '';
			var row = data['data'];
			
			if(errorCode == 0 || errorCode == 1){
				
				var i = 0;
				var colorClass = 'bg-blue';

				$.each(timeSlots, function(key, val){

					colorClass = 'bg-blue';
					if($.inArray(val, row) != -1){

						colorClass = 'bg-danger';
					}

					if(i == 0){

						strHtml += '\
						<div class="row p-t10">';
					}

					strHtml += '\
		                <div class="col-md-3">\
		                    <span class="label '+colorClass+' classApptTime curptr" id="'+key+'">'+val+'</span>\
		                </div>';

					++i;
		            if(i == 4){

						strHtml += '\
						</div>';

						i = 0;
					}

				});

			}else if(errorCode == 2){

				showAlertMessage('error', 'Error', data['errorMsg']);
			}

			$('#panelTimeSlot').html(strHtml);
		}
	)
}

$(document).on('click', '.classApptTime', function(e){

	e.preventDefault();
	var timeSlot = $(this).text();
	var date = $("#datepicker").val();

	var timeSlotId = $(this).attr('id');
	var enquirySeq = $('#idEnquirySeq').attr('data-id');
	var remark = $('#idTextRemark').val();

	if(confirm('Are you sure want to add appointment for '+timeSlot+' time.')){
		
		$.post('/user/services/processRequest',
			{
				cmd: 'createAppointment',
				date: date,
				enquiry_seq: enquirySeq,
				user_seq: userSeq,
				time_seq: timeSlot,
				remark: remark

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				
				if(errorCode == 0 && data['data']){

					showAlertMessage('success', 'Success', 'Enquriy has been assigned');
					setTimeout(function(){ window.top.location = '/user/enquiry-info?seq='+enquirySeq }, 4000);
				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}
});

$(document).on('click', '#idChangeStatus', function(e){

	var status = $('input[name="change_status"]:checked').val();
	var remark = $('#idTextRemark').val();

	if(empty(status)){

		showAlertMessage('error', 'Error', 'Please select any action');
		return false;
	}

	/*
	var feesInfo = [];
	$('input[name="fees_info"]:checked').each(function(){

		feesInfo.push($(this).val());
	});

	if((status == 'CL' || status == 'PT') && feesInfo.length == 0){

		showAlertMessage('error', 'Error', 'Please select any fee');
		return false;	
	}*/
	//feesInfo = JSON.stringify(feesInfo);
	updateStatus(status, remark, '', '');
});

function updateStatus(status, remark, feesInfo, prospectusNumber){

	var enquirySeq = $('#idEnquirySeq').attr('data-id');
	$.post('/user/services/processRequest',
		{
			cmd: 'updateStatus',
			status: status,
			enquiry_seq: enquirySeq,
			remark: remark,
			prospectus_no: prospectusNumber,
			fees_info: feesInfo

		},function(response){

			var data = $.parseJSON(response);
			var errorCode = data['errorCode'];
			
			if(errorCode == 0 && data['data']){

				showAlertMessage('success', 'Success', 'Enquriy status has been updated');
				setTimeout(function(){ window.top.location = '/user/home' }, 4000);
			}else{

				showAlertMessage('error', 'Error', data['errorMsg']);
			}
		}
	)
}

$(document).on('click', '#idCancelAppointment', function(e){

	var seq = $(this).attr('data-id');
	var enquirySeq = $('#idEnquirySeq').attr('data-id');

	if(confirm('Are you sure want to cancel the appointment')){

		$.post('/user/services/processRequest',
			{
				cmd: 'cancelAppointment',
				seq: seq,
				enquiry_seq: enquirySeq

			},function(response){

				var data = $.parseJSON(response);
				if(data['errorCode'] == 0){

					showAlertMessage('success', 'Success', 'Enquriy status has been cancelled');
					setTimeout(function(){ window.top.location = '/user/home' }, 4000);	
				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}
});

$(document).on('click', '#idCreateTest', function(e){
	e.preventDefault();

	var time = $('#idTimePicker').val();
	var date = $('#dateTestPicker').val();
	var enquirySeq = $('#idEnquirySeq').attr('data-id');

	$.post('/user/services/processRequest',
		{
			cmd: 'makeAppointmentForTest',
			enquiry_seq: enquirySeq,
			date: date,
			time: time

		},function(response){

			var data = $.parseJSON(response);
			var errorCode = data['errorCode'];
			
			if(errorCode == 0 && data['data']){

				showAlertMessage('success', 'Success', 'Appointment has been booked for written test');
				setTimeout(function(){ window.top.location = '/user/enquiry-info?seq='+enquirySeq }, 4000);
				
			}else{

				showAlertMessage('error', 'Error', data['errorMsg']);
			}
		}
	)
});

$(document).on('change', 'input[name="radio_test_result"]', function(){

	var result = $(this).val();
	var testSeq = $(this).attr('data-id');
	var enquirySeq = $('#idEnquirySeq').attr('data-id');

	if(result != ''){

		if(confirm('Are you sure want to assign result to the test')){

			$.post('/user/services/processRequest',
				{
					cmd: 'assignTestResult',
					test_seq: testSeq,
					result: result

				},function(response){

					var data = $.parseJSON(response);
					var errorCode = data['errorCode'];
					
					if(errorCode == 0 && data['data']){

						showAlertMessage('success', 'Success', 'Test Result has been assigned');
						setTimeout(function(){ window.top.location = '/user/enquiry-info?seq='+enquirySeq }, 4000);
					}else{

						showAlertMessage('error', 'Error', data['errorMsg']);
					}
				}
			)
		}
	}
});

$(document).on('change', 'input[name="re_test"]', function(){

	if($('input[name="re_test"]:checked').length){
		
		$('#idDivReAttempt').removeClass('hidden');

	}else{

		$('#idDivReAttempt').addClass('hidden');
	}
});

$(document).on('change', '.classEnquiryStatus', function(){

	var status = $('input[name="change_status"]:checked').val();
	var html = '';

	if(status != ''){

		if(status == 'CL'){

			html =
			'<div class="form-group col-md-12">\
                <div class="checkbox">\
                    <label>\
                        <input type="checkbox" name="fees_info" value="IT1">\
                        Installement I ( Rs.1000 )\
                    </label>\
                </div>\
            </div>\
            <div class="form-group col-md-12">\
                <div class="checkbox">\
                    <label>\
                        <input type="checkbox" name="fees_info" value="IT2">\
                        Installement II ( Rs.1000 )\
                    </label>\
                </div>\
            </div>\
            <div class="form-group col-md-12">\
                <div class="checkbox">\
                    <label>\
                        <input type="checkbox" name="fees_info" value="IT3">\
                        Installement III ( Rs.1000 )\
                    </label>\
                </div>\
            </div>';
		}

		$('.classFeeInfo').html(html);
		$('.classFeeInfo').removeClass('hidden');
	}
});

$(document).on('change', 'input[name="prospectus_taken"]', function(){

	if($('input[name="prospectus_taken"]:checked').length == 1){

		$('#idDivProspectusNo').removeClass('hidden');
		$('#idDivAdmissionTaken').removeClass('hidden');

	}else{
		
		$('input[name="prospectus_no"]').val('');
		$('#idDivProspectusNo').addClass('hidden');
		$('#idDivAdmissionTaken').addClass('hidden');		
	}
});

$(document).on('click', '#idCloseEnquiry', function(e){

	e.preventDefault();
	var isProspectusTaken = $('input[name="prospectus_taken"]:checked').length;
	var status = '';
	var prospectusNumber = $('input[name="prospectus_no"]').val();

	var remark = $('#idTextRemark').val();
	var feesInfo = [];
	

	if(isProspectusTaken == 0){

		showAlertMessage('error', 'Error', 'Please check prospectus taken');
		return false;

	}else{

		status = 'PT';
		feesInfo.push('PT');
	}

	if(empty(prospectusNumber)){

		showAlertMessage('error', 'Error', 'Please enter prospectus number');
		return false;

	}

	if($('.classEnquiryStatus:checked').length == 1){

		status = 'CL';
		$('input[name="fees_info"]:checked').each(function(){

			feesInfo.push($(this).val());
		});

		if(status == 'CL' && feesInfo.length == 0){

			showAlertMessage('error', 'Error', 'Please select any fee');
			return false;	
		}
	}

	feesInfo = JSON.stringify(feesInfo);
	updateStatus(status, remark, feesInfo, prospectusNumber);
})

$(document).on('click', '.classPrintReceipt', function(){

	$('#idFormPrintReceipt').submit();
});

/*$(document).on('click', '#idChangeEnquiryStatus', function(e){

	e.preventDefault();

	var status = $('.classChangeStatus:checked').val();
	var remark = $('#idTextRemark').val();

	updateStatus(status, remark, '', '');
})*/