$(document).ready(function(){ 
   
});
 
function getUrlParameter(name) 
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function initInputBox()
{
    var checkBox = $("input[type=checkbox]:not(.switchery), input[type=radio]:not(.no-uniform)");
    if (checkBox.size() > 0) {
        checkBox.each(function() {
            $(this).uniform();
        });
    };
}


function showAlertMessage(type, title, msg){

    var classToAdd = 'alert bg-success alert-styled-left';
    if(type == 'error'){

        classToAdd = 'alert bg-danger alert-styled-left';
    }else if(type == 'warning'){

        classToAdd = 'alert bg-warning alert-styled-left';
    }

    new PNotify({
        title: title,
        text: msg,
        delay: 3000,
        addclass: classToAdd,
        buttons: {
            sticker: false,
            closer_hover: false
        }
    });
}


function getConfirmObject(msg, type){

    var notice = new PNotify({
            title: 'Confirmation',
            text: '<p>'+msg+'</p>',
            hide: false,
            type: type,
            confirm: {
                confirm: true,
                buttons: [
                    {
                        text: 'Yes',
                        addClass: 'btn btn-sm btn-primary'
                    },
                    {
                        addClass: 'btn btn-sm btn-danger'
                    }
                ]
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        });

    return notice;
}


function hideOtherModal(id){ 
    $( ".modal" ).each(function( index ) {
      var curid= $( this ).attr('id');  
      showAlertMessage('#'+id,'H',''); 
      if(id != curid)
      {
        $( this ).modal('hide');
      }
      
    }); 
 }
 
function isValidEmail(email){

    if(empty(email))
    {
        return false;
    }
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function parseResponse(response, parentID, showToast){

    showToast = empty(showToast)? false : true;    
    var json = {};
    if(response != '')
    {
        json  = $.parseJSON(response);     
        if(json['errorMsg'] == 'RCP')
        {        
            window.top.location.href  =  '/admin/login?msg=1&action=login&done='+encodeURIComponent(window.top.location.href);
        }else if(showToast && json['errorCode'] != '0' && json['errorCode'] != '')
        {  
            displayToast('error','Error Occured!', json['errorMsg']);
            return false;
        }else if(json['errorCode'] != '0' && json['errorCode'] != '')
        {         
            showAlertMessage(parentID, 'E', json['errorMsg']); 
            return false;
        }
    }
    return json;
}

function random_int()
{
    return Math.floor(Math.random()*100000);
}

function isFunction(possibleFunction)
{
    return (typeof(possibleFunction) == typeof(Function));
}

function empty (mixed_var) {

    // Checks if the argument variable is empty
    // undefined, null, false, number 0, empty string,
    // string "0", objects without properties and empty arrays
    // are considered empty
    var undef, key, i, len;
    var emptyValues = [undef, undefined, null, false, 0, "", "0"];

    for (i = 0, len = emptyValues.length; i < len; i++) {

        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }

    if (typeof mixed_var === "object") {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            //if (mixed_var.hasOwnProperty(key)) {
            return false;
            //}
        }
        return true;
    }

    return false;
}


function isEmpty(str)
{
    if(!empty(str)){

        str += '';
        if(str == 'undefined') return false;
        str.replace(/^\s+|\s+$/g, '');
        return (str == '' || str == 0);
    }
    return true;
}


function contactusE()
{
    var a = '@'; var i = 'info';
    alert('Please contact us at : ' + i + a + DOMAIN_BASE_NAME);
}

function softReload()
{
  var param = "&random="+random_int();
  if(window.location.href.indexOf("?") < 0)
  {
      param = "?"+param;
  }
  window.location = window.location.href + param;
}

function softReloadTop()
{
  var param = "&random="+random_int();
  if(window.top.location.href.indexOf("?") < 0)
  {
      param = "?"+param;
  }
  window.top.location = window.top.location.href + param;
}

function parseUri (str) {

    var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),

		uri = {},
		i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";
    uri[o.q.name] = {};

    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		    if ($1) uri[o.q.name][$1] = $2;
	  });

	  return uri;
};

parseUri.options = {

    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
      	strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
      	loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};

function set_cookie ( name, value, exp_y, exp_m, exp_d, path, domain, secure )
{
    var cookie_string = name + "=" + escape ( value );
    if ( exp_y ) {
        var expires = new Date ( exp_y, exp_m, exp_d );
        cookie_string += "; expires=" + expires.toGMTString();
    }
    if ( path )
        cookie_string += "; path=" + escape ( path );
    if ( domain )
        cookie_string += "; domain=" + escape ( domain );
    if ( secure )
        cookie_string += "; secure";
    document.cookie = cookie_string;
}

function createCookie(name,value,days)
{
    if(days)
    {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/; domain=."+DOMAIN_BASE_NAME;
}

function readCookie(name)
{
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++)
    {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name)
{
    createCookie(name,"",-1);
}

function eraseAllCookies()
{
    eraseCookie("user");
    eraseCookie("persist");
    eraseCookie("lastlogin");
    eraseCookie("login");
    eraseCookie("userwork");
    eraseCookie("seq");
    eraseCookie("fname");
    eraseCookie("when");
    eraseCookie("gender");
    eraseCookie("cart");
    eraseCookie("cart");
    eraseCookie("totcitems");
}

function cleanAllCookies()
{
    eraseAllCookies(); //calling this twice to take care of dirty cookies problems
    eraseAllCookies();
}

function readCookieData(name)
{
    var data = readCookie(name);
    //alert(data);
    if(data != null)
    {
        data = decodeURIComponent(data);
        var dArr = data.split('&');
        data = {};
        for(var i=0; i<dArr.length; ++i)
        {
            //alert(dArr[i]);
            var vals = dArr[i].split('=');
            if(vals.length > 1)
            {
                data[decodeURIComponent(vals[0])] = decodeURIComponent(vals[1]);
            }
            else if(vals.length == 1)
            {
                data[decodeURIComponent(vals[0])] = '';
            }
        }
    }
    else
    {
        data = {};
    }
  return data;
}

function setCookieData(name, data, days)
{
    var str = '';
    for(var key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if(str == '')
            {
                str = encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
            }
            else
            {
                str = str + '&' + encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
            }
        }
    }
    createCookie(name,encodeURIComponent(str),days);
}


function removeDataItem(data, key)
{
    if (!data.hasOwnProperty(key))
        return;
    if (isNaN(parseInt(key)) || !(data instanceof Array))
        delete data[key];
    else
      data.splice(key, 1);
}


function testCookieData()
{
  data = {};
  data['a']='a';
  data['b']='b';
  data['c']='c';
  data['d=d'] = 'd&d';

  setCookieData('abc', data, 1);
  setCookieData('mno', data, 2);

  d = readCookieData('mno');
  console.log(d);
  alert('will remove c');
  removeDataItem(d, 'c');
  setCookieData('mno', d, 1);
  console.log(d);
  alert(readCookie('mno'));
}


function resizeHhWwImg(selectorTxt)
{
    $(selectorTxt).each(function() {
      var width = $(this).width();    // Current image width
      var height = $(this).height();  // Current image height
      if(width > 0 && height > 0)
      {
        if(width>height)
        {
          $(this).removeClass('hh');
          $(this).addClass('ww');
        }
      }
    });
}

function stopPropagationE(e)
{
  if(window.event !== undefined)
    window.event.cancelBubble = true;
  else
    e.stopPropagation();
}

function showRecaptcha(elementId)
{
   Recaptcha.create("6Ldw79kSAAAAABL_w4Nhhm3NwlglqfAH7bNRrc8k", elementId, {
     theme: "red",
    callback: Recaptcha.focus_response_field});
}


function getDoneUrl()
{
  var uri = parseUri(window.location);
  //console.log(uri);
  var d = uri.queryKey['done'];
  if(d != undefined)
  {
    d = decodeURIComponent(d);
  }
  if(d == undefined || d == '' || d == ' ')
  {
    d = '/';
  }
  return d;
}

function gotoDoneUrl()
{
  var d = getDoneUrl();
  if(d == '/')
  {
    d = '/my/home';
  }
  window.top.location = d;
}

function gotoLink(url)
{
  window.top.location = url;
  return false;
}

function getJsonObject(response)
{
    var json = {};               
    if(response != '')
    {
      json  = $.parseJSON(response);     
    }
    return json;
}

/******** For Blocked New Sign Up Form Submission Fn Ends************/



function blockUI(item) 
{    
    
    if(empty(item))
    {
        $.blockUI({
            message: '<img src="../images/reload.gif" style="width:50px; max-width:50px;">',
            css: {
                border: 'none',
                padding: '0px',
                width: '20px', 
                left: '50%', 
                height: '20px',
                backgroundColor: 'transparent'
            },
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.6,
                position: 'fixed',
                cursor: 'wait'
            }
        });
            
    }else
    {
        item = empty(item)? $('body') : item; 
        $(item).block({
            message: '<img src="../images/reload.gif" style="width:20px; max-width:20px;">',
            css: {
                border: 'none',
                padding: '0px',
                width: '20px',
                left: '50%', 
                height: '20px',
                backgroundColor: 'transparent'
            },
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.6,
                position: 'fixed',
                cursor: 'wait'
            }
        });
    }            
        
         
    }
    
    function unblockUI(item) 
    {
        if(empty(item))
        {
             $.unblockUI();
        }
        else
        {
            item = empty(item)? $('body') : item;
            $(item).unblock();
        }
        
    }  
 
function logoutUser()
{ 
      $.post('/admin/services/authenticate', {cmd:"logoutUser"},
      function(response)
      {               
        if(parseResponse(response))
        {  
             window.top.location.href = '/';
        }
         
      }); 
} 
