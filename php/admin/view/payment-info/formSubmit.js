$(document).on('click', '.classSpanPrint', function(e){

	var seq = $(this).attr('data-seq');
	$('input[name="payment_seq"]').val(seq);
	$('#idFormPrintReceipt').submit();
})


/*var searchBy;
var searchKey = [];
var pageNo = 1;
var last = 0;

$(document).ready(function(){

	var param = getUrlParameter('status');
	if(!empty(param)){

		searchBy = 'ST';
		searchKey = param;
	}
});

var date = new Date();
$(function(){
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});
});

$(document).on('change', '#idSelectSearchBy', function(){

	searchBy = 0;
	searchBy = $(this).val();

	valueFromId = '';
	$('.classSearchOptions').addClass('hidden');

	if(searchBy != 0){

		if(searchBy == 'DT'){

			$('#idDivFromSearchDate').removeClass('hidden');
			$('#idDivToSearchDate').removeClass('hidden');

		}else if(searchBy == 'RN' || searchBy == 'NM'){

			$('#idDivSearchAll').removeClass('hidden');
		}

	}else if(searchBy == 0){

		$('input[name="search_key"]').val('');
	}
});


function getSearchResult(searchBy, searchKey, isModal){

	
	//blockUI();
	$.post('/admin/services/processRequest',
		{
			cmd: 'getSearchResult',
			search_by: searchBy,
			search_key: searchKey,
			status: status,
			page_no: pageNo,
			formatted_response: 1

		},function(response){

			var data = $.parseJSON(response);
			last = data['data'].length < 20 ? 1 : last;

			if(isModal){

				showInfoModal(data);
			}else{

				showMainTable(data);
			}

			unblockUI();
		}
	)
}

$(document).on('click', '#idButtonSearch', function(e){

	e.preventDefault();
	// if(empty(searchBy)){

	// 	showAlertMessage('error', 'Error', 'Please select any search criteria');
	// 	return false;
	// }

	searchKey = [];
	if(searchBy == 'DT'){

		var fromDate = $('#idInputFromSearchDate').val();
		var toDate  = $('#idInputToSearchDate').val();

		searchKey.push(fromDate);
		searchKey.push(toDate);

	}else{

		searchKey.push($('input[name="search_key"]').val());


	}

	// if(empty(searchKey)){

	// 	showAlertMessage('error', 'Error', 'Please select any search keyword');
	// 	return false;
	// }

	getSearchResult(searchBy, searchKey, 0);
	// if(!empty(searchBy) && !empty(searchKey)){

	// 	getSearchResult(searchBy, searchKey);
	// }

});

$(document).on('click', '.classNavigation', function(e){

	e.preventDefault();
	var action = $(this).attr('data-action');

	if(action == 'N'){

		if(last){

			showAlertMessage('warning', 'Warning', 'No more reords found');
			return false;
		}
		++pageNo;

	}else if(action == 'P'){

		--pageNo;
		if(pageNo < 1){

			pageNo = 1;
			showAlertMessage('warning', 'Warning', 'This is first page');
			return false;
		}
	}

	getSearchResult(searchBy, searchKey);
})*/

/*$(document).on('click', '.jsClassDelete', function(e){

	e.preventDefault();
	var enquiryId = $(this).attr('data-id');

	//if(confirm('Are sure to delete the enquiry record. All the information '))
	var msg = 'Are sure to delete the enquiry record. All the information related to enquiry will be erased.';
	var type = 'danger';
	var notice  = getConfirmObject(msg, type);

	// On confirm
    notice.get().on('pnotify.confirm', function() {
        
        deleteEnquiry(enquiryId);
    })

    // On cancel
    notice.get().on('pnotify.cancel', function() {
        return false;
    });
})


function deleteEnquiry(enquiryId){

	if(!empty(enquiryId)){

		$.post('/user/services/processRequest',
			{
				cmd: 'deleteEnquiry',
				enquiry_seq: enquiryId

			},function(response){

				var data = $.parseJSON(response);
				if(data['errorCode'] == 0){

					showAlertMessage('success', 'Success', 'Enquiry record has been deleted.');
					getSearchResult(searchBy, searchKey);
				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
					return false;
				}
			}
		)
	}
}*/

/*$(document).on('click', '.classLiCheckOut', function(e){

	e.preventDefault();

	var bookingSeq = $(this).attr('data-seq');
	if(!empty(bookingSeq)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'checkout',
				booking_seq: bookingSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];

				if(errorCode == 0){

					$('input[name="booking_seq"]').val(bookingSeq);
					$('#idFormPayment').submit();

				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}
});


$(document).on('click', '.classCancelBooking', function(e){

	e.preventDefault();

	var bookingSeq = $(this).attr('data-id');
	if(!empty(bookingSeq)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'changeBookingStatus',
				booking_seq: bookingSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];

				if(errorCode == 0){

					showAlertMessage('success', 'Success', 'Booking has been cancelled');
					setTimeout(function(){ window.top.location = '/admin/booking' }, 4000);

				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}
});


function showMainTable(data){

	var html = '';
	var cancelButton;
    var checkoutButton;
    var editButton;


	$('#idTbodyBookingInfo').html('');
	last = 0;
	var status = '';
	
	if(data['errorCode'] == 0){

		$.each(data['data'], function(key, val){

			cancelButton = '';
            checkoutButton = '';
            editButton = '<td>'+val['name']+'</td>';

            var checkInTimeStamp = 0;
            var currentTimestamp = new Date().valueOf();

            if(val['orig_status'] == 'A'){
                
                cancelButton = '<span data-popup="tooltip" title="Cancel" data-placement="bottom" class="curptr classCancelBooking label bg-blue padding4 margin-right6" data-id="'+val['booking_seq']+'"><i class=" icon-cancel-square"></i></span>';
                editButton = '<td><a href="/admin/add-booking?edit='+val['booking_seq']+'">'+val['name']+'</a></td>';

                checkInTimeStamp = new Date(val['checkin_date_orig']+' 00:00:00');
                if(checkInTimeStamp <= currentTimestamp){

                    checkoutButton = '<span data-popup="tooltip" title="Checkout" data-placement="bottom" class="curptr classLiCheckOut label bg-blue padding4" data-seq="'+val['booking_seq']+'"><i class="icon-arrow-right6"></i></span>';
                }
            }
			
			html += '<tr>\
                <td><a class="classViewInfo" data-id="'+val['reservation_no']+'">'+val['reservation_no']+'</a></td>\
                '+editButton+'\
                <td>'+val['checkin_date']+'</td>\
                <td>'+val['checkin_time']+'</td>\
                <td>'+val['number_of_nights']+'</td>\
                <td>'+val['number_of_rooms']+'</td>\
                <td><span class="'+val['label_class']+' classWidth80">'+val['status']+'</span></td>\
                <td>';
                    html += cancelButton;
                    html += checkoutButton;
                html += '\
                </td>\
            </tr>';
		});

	}else{

		html += '<tr><th colspan="7"> No Records Found.</th></tr>';
	}

	$('#idTbodyBookingInfo').html(html);
	$('.classPageNumber').html(pageNo);
}


$(document).on('click', '.classViewInfo', function(e){

	e.preventDefault();
	id = $(this).attr('data-id');

	var searchKey = [];
	searchKey.push(id);
	getSearchResult('RN', searchKey, 1);
})

function showInfoModal(data){

	if(data['errorCode'] == 0){

		data = data['data'][0];
		$('#idDivBookingInfo').html('');
		
		var strHtml = 
		'<div class="row padding-b10">\
	        <div class="col-md-12 fontBold">\
	             Name\
	        </div>\
	        <div class="col-md-12">\
				'+data['fname']+' '+data['lname']+'\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Mobile No.</span>\
	            <div>\
	            	'+data['mobile']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	            <span class="fontBold">Email Id</span>\
	            <div>\
	               '+data['email_id']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">City</span>\
	            <div>\
	                '+data['city']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	            <span class="fontBold">Country</span>\
	            <div>\
	               '+data['nationality']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Reservation Number</span>\
	            <div>\
	            '+data['reservation_no']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	            <span class="fontBold">Total Person</span>\
	            <div>\
	               '+(data['number_of_adults'] + data['number_of_childs'])+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Check In Date</span>\
	            <div>\
	                '+data['checkin_date']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	           <span class="fontBold">Check In Time</span>\
	            <div>\
	               '+data['checkin_time']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Number Of Nights</span>\
	            <div>\
	                '+data['number_of_nights']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	           <span class="fontBold">Number Of Rooms</span>\
	            <div>\
	               '+data['number_of_rooms']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Room Type</span>\
	            <div>\
	                '+data['room_name']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	           <span class="fontBold">Status</span>\
	            <div>\
	               '+data['status']+'\
	            </div>\
	        </div>\
	    </div>';


	    $('#idDivBookingInfo').html(strHtml);
	    $('#idModalBookingInfo').modal('show');
	}

}*/