var searchBy;
var searchKey = [];
var pageNo = 1;
var last = 0;

$(document).ready(function(){

	var param = getUrlParameter('status');
	if(!empty(param)){

		searchBy = 'ST';
		searchKey = param;
	}
});

var date = new Date();
$(function(){
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});
});

$(document).on('change', '#idSelectSearchBy', function(){

	searchBy = 0;
	searchBy = $(this).val();

	valueFromId = '';
	$('.classSearchOptions').addClass('hidden');

	if(searchBy != 0){

		if(searchBy == 'DT'){

			$('#idDivFromSearchDate').removeClass('hidden');
			$('#idDivToSearchDate').removeClass('hidden');

		}else if(searchBy == 'RN' || searchBy == 'NM'){

			$('#idDivSearchAll').removeClass('hidden');
		}

	}else if(searchBy == 0){

		$('input[name="search_key"]').val('');
	}
});


function getSearchResult(searchBy, searchKey, isModal){

	
	blockUI();
	$.post('/admin/services/processRequest',
		{
			cmd: 'getSearchResult',
			search_by: searchBy,
			search_key: searchKey,
			status: status,
			page_no: pageNo,
			formatted_response: 1

		},function(response){

			var data = $.parseJSON(response);
			last = data['data'].length < 20 ? 1 : last;

			if(isModal){

				showInfoModal(data);
			}else{

				showMainTable(data);
			}

			unblockUI();
		}
	)
}

$(document).on('click', '#idButtonSearch', function(e){

	e.preventDefault();
	// if(empty(searchBy)){

	// 	showAlertMessage('error', 'Error', 'Please select any search criteria');
	// 	return false;
	// }

	searchKey = [];
	if(searchBy == 'DT'){

		var fromDate = $('#idInputFromSearchDate').val();
		var toDate  = $('#idInputToSearchDate').val();

		searchKey.push(fromDate);
		searchKey.push(toDate);

	}else{

		searchKey.push($('input[name="search_key"]').val());


	}

	// if(empty(searchKey)){

	// 	showAlertMessage('error', 'Error', 'Please select any search keyword');
	// 	return false;
	// }

	getSearchResult(searchBy, searchKey, 0);
	// if(!empty(searchBy) && !empty(searchKey)){

	// 	getSearchResult(searchBy, searchKey);
	// }

});

$(document).on('click', '.classNavigation', function(e){

	e.preventDefault();
	var action = $(this).attr('data-action');

	if(action == 'N'){

		if(last){

			showAlertMessage('warning', 'Warning', 'No more reords found');
			return false;
		}
		++pageNo;

	}else if(action == 'P'){

		--pageNo;
		if(pageNo < 1){

			pageNo = 1;
			showAlertMessage('warning', 'Warning', 'This is first page');
			return false;
		}
	}

	getSearchResult(searchBy, searchKey);
})

/*$(document).on('click', '.jsClassDelete', function(e){

	e.preventDefault();
	var enquiryId = $(this).attr('data-id');

	//if(confirm('Are sure to delete the enquiry record. All the information '))
	var msg = 'Are sure to delete the enquiry record. All the information related to enquiry will be erased.';
	var type = 'danger';
	var notice  = getConfirmObject(msg, type);

	// On confirm
    notice.get().on('pnotify.confirm', function() {
        
        deleteEnquiry(enquiryId);
    })

    // On cancel
    notice.get().on('pnotify.cancel', function() {
        return false;
    });
})


function deleteEnquiry(enquiryId){

	if(!empty(enquiryId)){

		$.post('/user/services/processRequest',
			{
				cmd: 'deleteEnquiry',
				enquiry_seq: enquiryId

			},function(response){

				var data = $.parseJSON(response);
				if(data['errorCode'] == 0){

					showAlertMessage('success', 'Success', 'Enquiry record has been deleted.');
					getSearchResult(searchBy, searchKey);
				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
					return false;
				}
			}
		)
	}
}*/

$(document).on('click', '.classLiCheckOut', function(e){

	e.preventDefault();
	var data = $(this).data();
	var newMinDate = new Date(data['check']);
	newMinDate.setDate(newMinDate.getDate() + 1)

	var date = new Date();
	$(function(){
		$('#idDivCheckOutDate').datepicker({
			dateFormat: "yy-mm-dd",
			minDate: newMinDate.getFullYear()+'-'+(newMinDate.getMonth() + 1)+'-'+newMinDate.getDate(),
			maxDate: date
		});
	});
	$('.pickatime').pickatime();


	$('#idDivCheckOutDate').val(date.getFullYear()+'-'+(date.getMonth() + 1)+'-'+date.getDate());
	$('.pickatime').val(getTimeWithFormat(date));
	$('#buttonCheckOut').attr('data-seq', data['seq']);

	$('#idModalCheckout').modal('show');
});

//code to destroy the datepicker
$('#idModalCheckout').on('hidden.bs.modal', function(e){
	
	$("#idDivCheckOutDate").datepicker("destroy");
	$('#buttonCheckOut').attr('data-seq', 0);
});


$(document).on('click', '#buttonCheckOut', function(){

	var checkoutDate = $('#idDivCheckOutDate').val(); 
	var checkoutTime = $('.pickatime').val(); 

	var bookingSeq = $(this).attr('data-seq');
	if(!empty(bookingSeq)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'checkout',
				booking_seq: bookingSeq,
				checkout_date: checkoutDate,
				checkout_time: checkoutTime

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];

				if(errorCode == 0){

					$('input[name="booking_seq"]').val(bookingSeq);
					$('#idFormPayment').submit();

				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}
})

$(document).on('click', '.classCancelBooking', function(e){

	e.preventDefault();

	var bookingSeq = $(this).attr('data-id');
	if(!empty(bookingSeq)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'changeBookingStatus',
				booking_seq: bookingSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];

				if(errorCode == 0){

					showAlertMessage('success', 'Success', 'Booking has been cancelled');
					setTimeout(function(){ window.top.location = '/admin/booking' }, 4000);

				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}
});


function showMainTable(data){

	var html = '';
	var cancelButton;
    var checkoutButton;
    var editButton;
    var roomNumber;

    var checkInTimeStamp;
	var currentTimestamp;
	var bookingSeq;

	$('#idTbodyBookingInfo').html('');
	last = 0;
	var status = '';
	
	if(data['errorCode'] == 0){

		$.each(data['data'], function(key, val){

			cancelButton = '';
            checkoutButton = '';
            editButton = '<td>'+val['name']+'</td>';
            infoButton = '';

            checkInTimeStamp = 0;
            currentTimestamp = new Date().valueOf();
            bookingSeq = val['booking_seq'];

            var roomNumber = val['room_numbers'].replace(', ', '<br>');

            if(val['orig_status'] == 'A' || val['orig_status'] == 'CI'){

                cancelButton = '<span data-popup="tooltip" title="Cancel" data-placement="bottom" class="curptr classCancelBooking label bg-danger padding4 margin-right6" data-id="'+bookingSeq+'"><i class=" icon-cancel-square"></i></span>';
                editButton = '<td><a href="/admin/add-booking?edit='+val['booking_seq']+'">'+val['name']+'</a></td>';
                
                checkInTimeStamp = new Date(val['checkin_date_orig']+' 00:00:00');
                if(checkInTimeStamp == currentTimestamp){

                    infoButton = '<span data-popup="tooltip" title="CheckIn" data-placement="bottom" class="curptr classSpanCheckIn label bg-success-400 padding4 margin-right6" data-id="'+bookingSeq+'" data-room="'+val['number_of_rooms']+'"><i class="icon-arrow-left52"></i></span>';
                }
                
                if(val['orig_status'] == 'CI'){

                    cancelButton = '';
                    editButton = '<td>'+val['name']+'</td>';
                    infoButton = '<a class="classViewInfo" data-id="'+val['reservation_no']+'">'+roomNumber+'</a>';
                    if(checkInTimeStamp <= currentTimestamp){

                        checkoutButton = '<span data-popup="tooltip" title="Checkout" data-placement="bottom" class="curptr classLiCheckOut label bg-blue padding4" data-seq="'+bookingSeq+'" data-check="'+val['checkin_date_orig']+'"><i class="icon-arrow-right6"></i></span>';
                    }
                }

            }
			
			html += '<tr>\
                <td>'+infoButton+'</td>\
                '+editButton+'\
                <td>'+val['checkin_date']+'</td>\
                <td>'+val['checkin_time']+'</td>\
                <td>'+val['number_of_nights']+'</td>\
                <td>'+val['number_of_rooms']+'</td>\
                <td><span class="'+val['label_class']+' classWidth80">'+val['status']+'</span></td>\
                <td>';
                    html += cancelButton;
                    html += checkoutButton;
                html += '\
                </td>\
            </tr>';
		});

	}else{

		html += '<tr><th colspan="8"> No Records Found.</th></tr>';
	}

	$('#idTbodyBookingInfo').html(html);
	$('.classPageNumber').html(pageNo);
}


$(document).on('click', '.classViewInfo', function(e){

	e.preventDefault();
	id = $(this).attr('data-id');

	var searchKey = [];
	searchKey.push(id);
	getSearchResult('RN', searchKey, 1);
})

function showInfoModal(data){

	if(data['errorCode'] == 0){

		data = data['data'][0];
		$('#idDivBookingInfo').html('');
		
		var strHtml = 
		'<div class="row padding-b10">\
	        <div class="col-md-12 fontBold">\
	             Name\
	        </div>\
	        <div class="col-md-12">\
				'+data['fname']+' '+data['lname']+'\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Mobile No.</span>\
	            <div>\
	            	'+data['mobile']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	            <span class="fontBold">Email Id</span>\
	            <div>\
	               '+data['email_id']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">City</span>\
	            <div>\
	                '+data['city']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	            <span class="fontBold">Country</span>\
	            <div>\
	               '+data['nationality']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Reservation Number</span>\
	            <div>\
	            '+data['reservation_no']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	            <span class="fontBold">Total Person</span>\
	            <div>\
	               '+(data['number_of_adults'] + data['number_of_childs'])+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Check In Date</span>\
	            <div>\
	                '+data['checkin_date']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	           <span class="fontBold">Check In Time</span>\
	            <div>\
	               '+data['checkin_time']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Number Of Nights</span>\
	            <div>\
	                '+data['number_of_nights']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	           <span class="fontBold">Number Of Rooms</span>\
	            <div>\
	               '+data['number_of_rooms']+'\
	            </div>\
	        </div>\
	    </div>\
	    <div class="row padding-b10">\
	        <div class="col-md-6">\
	            <span class="fontBold">Room Type</span>\
	            <div>\
	                '+data['room_name']+'\
	            </div>\
	        </div>\
	        <div class="col-md-6">\
	           <span class="fontBold">Status</span>\
	            <div>\
	               '+data['status']+'\
	            </div>\
	        </div>\
	    </div>';


	    $('#idDivBookingInfo').html(strHtml);
	    $('#idModalBookingInfo').modal('show');
	}

}


function getTimeWithFormat(timeStamp){

	var date = new Date(timeStamp);
    var hours = date.getHours();
    var date1 = date.getDate();
    var month = date.getMonth()+1;
    var year = date.getFullYear();
  
    var minutes = date.getMinutes();
    var format = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;

    hours = hours < 10 ? '0'+hours : hours;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + format;

    return strTime;
}

$(document).on('click', '.classSpanCheckIn', function(e){

	e.preventDefault();
	var seq = $(this).attr('data-id');

	$('#idDivCheckIn').html('');
	if(!empty(seq)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'getRoomNumbers',
				booking_seq: seq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				
				if(errorCode == 0){
					
					var info = data['data'];
					var strHtml = '';
					var i = 1;

					if(!empty(info)){

						var dataLen = info.length;
						strHtml = '\
						<div class="row">\
							<div class="form-group">';
								$.each(info, function(key, val){

									strHtml += '\
									<div class="col-md-3">\
		                                <div class="checkbox">\
		                                    <label class="checkbox-inline">\
		                                        <input type="checkbox" class="styled classRoomNumber" name="room_number" value="'+val['room_no_seq']+'">\
		                                        '+val['room_no']+'\
		                                    </label>\
		                                </div>\
		                            </div>';

		                            ++i;
		                            if(i % 5 == 0 && i <= dataLen){
										
										strHtml += '\
											</div>\
										</div>\
										<div class="row">\
											<div class="form-group">';		                            	
		                            }
								});

						strHtml += '\
							</div>\
						</div>';
					}

					$('#idDivCheckIn').html(strHtml);
				}
				unblockUI();
			}
		)
	}


	$('#idModalCheckIn').modal('show');
})

$(document).on('click', '#buttonCheckIn', function(e){
	
	var roomNumbers = [];
	var data = $('.classSpanCheckIn').data();

	var bookingSeq = data['id']
	var totalRooms = data['room']

	$('.classRoomNumber').each(function(index, value){

		if($(this).prop('checked') == 1){

			roomNumbers.push($(this).val());
		}
	});


	if(empty(roomNumbers)){

		showAlertMessage('error', 'Error', 'Please select any room number for checkin');
		return false;
	}

	if(roomNumbers.length != totalRooms){

		showAlertMessage('error', 'Error', 'Please select only '+totalRooms+' rooms');
		return false;
	}

	var strRoomNumbers = JSON.stringify(roomNumbers);
	if(!empty(bookingSeq)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'checkInUser',
				booking_seq: bookingSeq,
				room_numbers: strRoomNumbers

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				
				$('#idModalCheckIn').modal('hide');
				if(errorCode == 0){
					
					showAlertMessage('success', 'Success', 'Usre has been checkedIn');
					
				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
					return false;				
				}
			}
		)
	}
});

$('#idModalCheckIn').on('hidden.bs.modal', function(e){
	
	$('#idDivCheckIn').html('');
});