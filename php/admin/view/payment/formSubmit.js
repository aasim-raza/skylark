$(document).ready(function(){

});



$(document).on('change', '.classTax', function(){

	var dataId = $(this).attr('data-id');
	var taxVal = $(this).val();
	var amount = $('#idSpanBillAmount_'+dataId).html();

	if(empty(taxVal)){

		$('#idSpanAmount_'+dataId).attr('data-tax', 1);
		showAlertMessage('warning', 'Warning', 'Tax value can not be empty');
		return false;
	}

	amount  = parseInt(amount);
	taxVal = parseInt(taxVal);

	var totalAmount = calculateTotalAmount(amount, taxVal);
	$('#idSpanAmount_'+dataId).html(totalAmount);


	$('#idSpanAmount_'+dataId).attr('data-tax', taxVal);
	$('#idSpanAmount_'+dataId).attr('data-total_amount', totalAmount);

	var finalAmount = 0;
	$('.classSpanAmount').each(function(){

		finalAmount += parseInt($(this).html());
	});

	$('#idSpanTotalAmount').html(finalAmount);

});

function calculateTotalAmount(amount, taxVal){

	taxVal = taxVal == 0 ? 1 : taxVal;
	var totalAmount = (amount * taxVal) / 100;

	totalAmount = totalAmount + amount;
	return totalAmount;
}



$(document).on('click', '#idButtonSubmitPayment', function(e){

	var objAmount = [];
	var bookingSeq = $(this).attr('data-seq');

	var tax = 0;
	var numberCounter = 0;

	$('.classSpanAmount').each(function(){

		tax = parseInt($(this).attr('data-tax'));
		objAmount.push({

			tax: tax,
			amount: $(this).attr('data-amount'),
			service_seq: $(this).attr('data-service_seq'),
			total_amount: $(this).attr('data-total_amount')
		});
		
	});
	
	var strAmount = JSON.stringify(objAmount);
	if(!empty(objAmount)){

		$.post('/admin/services/processRequest',
			{
				cmd: 'makePayment',
				booking_seq: bookingSeq,
				data: strAmount

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];

				if(errorCode == 0){

					showAlertMessage('success', 'Success', 'Payment has been save successfuly');
					setTimeout(function(){ window.top.location = '/admin/booking' }, 4000);

				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
				}
			}
		)
	}

});

function printDiv(divName) {

    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}