var searchBy;
var searchKey = [];
var pageNo = 1;
var last = 0;
var status = '';

$(document).ready(function(){

	var param = getUrlParameter('action');
	var date = new Date();

	if(param == 'arrival'){
		
		$(function(){
			$(".datepicker").datepicker({
				dateFormat: "yy-mm-dd",
				minDate: date
			});
		});		

		status = 'A';

	}else{
		
		$(function(){
			$(".datepicker").datepicker({
				dateFormat: "yy-mm-dd"
			});
		});

		status = 'C';
	}
});



$(document).on('change', '#idSelectSearchBy', function(){

	searchBy = 0;
	searchBy = $(this).val();

	valueFromId = '';
	$('.classSearchOptions').addClass('hidden');

	if(searchBy != 0){

		if(searchBy == 'DT'){

			$('#idDivFromSearchDate').removeClass('hidden');
			$('#idDivToSearchDate').removeClass('hidden');

		}else if(searchBy == 'RN' || searchBy == 'NM'){

			$('#idDivSearchAll').removeClass('hidden');
		}

	}else if(searchBy == 0){

		$('input[name="search_key"]').val('');
	}
});


function getSearchResult(searchBy, searchKey){

	var html = '';
	$('#idTbodyBookingInfo').html('');
	last = 0;

	var action = bookingSeq = getUrlParameter('action');
	var sortBy = '';
	sortBy = action == 'arrival' ? 'checkin_date' : '';

	$.post('/admin/services/processRequest',
		{
			cmd: 'getSearchResult',
			search_by: searchBy,
			search_key: searchKey,
			status: status,
			page_no: pageNo,
			formatted_response: true,
			sort_by: sortBy

		},function(response){

			var data = $.parseJSON(response);
			last = data['data'].length < 20 ? 1 : last;
			
			if(data['errorCode'] == 0){

				$.each(data['data'], function(key, val){

					
					html += '<tr>\
	                    <td><a href="/admin/booking-info?seq='+val['booking_seq']+'">'+val['reservation_no']+'</a></td>\
	                    <td>'+val['name']+'</td>\
	                    <td>'+val['checkin_date']+'</td>\
	                    <td>'+val['checkin_time']+'</td>\
	                    <td>'+val['number_of_nights']+'</td>\
	                    <td>'+val['number_of_rooms']+'</td>\
	                    <td>'+val['status']+'\
	                    </td>\
	                </tr>';
				});

			}else{

				html += '<tr><th colspan="7"> No Records Found.</th></tr>';
			}

			$('#idTbodyBookingInfo').html(html);
			$('.classPageNumber').html(pageNo);
		}
	)
}

$(document).on('click', '#idButtonSearch', function(e){

	e.preventDefault();
	// if(empty(searchBy)){

	// 	showAlertMessage('error', 'Error', 'Please select any search criteria');
	// 	return false;
	// }

	// searchKey = [];
	// if(searchBy == 'DT'){

	// 	var fromDate = $('#idInputFromSearchDate').val();
	// 	var toDate  = $('#idInputToSearchDate').val();

	// 	searchKey.push(fromDate);
	// 	searchKey.push(toDate);

	// }else{

	// 	searchKey.push($('input[name="search_key"]').val());


	// }

	// if(empty(searchKey)){

	// 	showAlertMessage('error', 'Error', 'Please select any search keyword');
	// 	return false;
	// }	

	// if(!empty(searchBy) && !empty(searchKey)){

	// 	getSearchResult(searchBy, searchKey);
	// }

	searchKey = [];
	var fromDate = $('#idInputFromSearchDate').val();
	var toDate  = $('#idInputToSearchDate').val();

	searchKey.push(fromDate);
	searchKey.push(toDate);

	searchBy = 'DT';
	getSearchResult(searchBy, searchKey);
});

$(document).on('click', '.classNavigation', function(e){

	e.preventDefault();
	var action = $(this).attr('data-action');

	if(action == 'N'){

		if(last){

			showAlertMessage('warning', 'Warning', 'No more reords found');
			return false;
		}
		++pageNo;

	}else if(action == 'P'){

		--pageNo;
		if(pageNo < 1){

			pageNo = 1;
			showAlertMessage('warning', 'Warning', 'This is first page');
			return false;
		}
	}

	getSearchResult(searchBy, searchKey);
})

$(document).on('click', '.jsClassDelete', function(e){

	e.preventDefault();
	var enquiryId = $(this).attr('data-id');

	//if(confirm('Are sure to delete the enquiry record. All the information '))
	var msg = 'Are sure to delete the enquiry record. All the information related to enquiry will be erased.';
	var type = 'danger';
	var notice  = getConfirmObject(msg, type);

	// On confirm
    notice.get().on('pnotify.confirm', function() {
        
        deleteEnquiry(enquiryId);
    })

    // On cancel
    notice.get().on('pnotify.cancel', function() {
        return false;
    });
})


function deleteEnquiry(enquiryId){

	if(!empty(enquiryId)){

		$.post('/user/services/processRequest',
			{
				cmd: 'deleteEnquiry',
				enquiry_seq: enquiryId

			},function(response){

				var data = $.parseJSON(response);
				if(data['errorCode'] == 0){

					showAlertMessage('success', 'Success', 'Enquiry record has been deleted.');
					getSearchResult(searchBy, searchKey);
				}else{

					showAlertMessage('error', 'Error', data['errorMsg']);
					return false;
				}
			}
		)
	}
}