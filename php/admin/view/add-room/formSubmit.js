var hotelSeq = 0;
var hotelImages = [];

var facilities = [];
var isEdit = false;
var _roomSeq = 0;


$(document).ready(function(){

	$('.pickatime').pickatime();
	$('#idFormAddRoom').validate();

	hotelSeq = $('#idHotelSeq').attr('data-id');
	var roomSeq = getUrlParameter('edit');
	if(!empty(roomSeq)){

		_roomSeq = roomSeq;
		getEditInterface(roomSeq);
		isEdit = true;	
	}

	initializeDropZone();
});


function initializeDropZone(){

	var fileOptions = {}; 
    fileOptions[0] = ['main', '800','600','90'];
   
	var myDropzone = new Dropzone("#dropzone_file_limits", 
									{ 
										url: "/admin/services/processRequest",
										headers: { "cmd": "uploadImage", "params": JSON.stringify(fileOptions), "folder": "room"},
										paramName: "file", // The name that will be used to transfer the file
								        dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
								        maxFilesize: 0.4, // MB
								        maxFiles: 4,
								        maxThumbnailFilesize: 1,
								        acceptedFiles : "image/png, image/jpeg, image/bmp",
								        addRemoveLinks: true,
								        success: function(file, serverResponse) {


											var data = $.parseJSON(serverResponse);
											var url = '';
											var fileName = file['name'];

											if(data['errorCode'] == 0 && !empty(data['data'])){

												var imageData = data['data'];
												url = imageData[fileName]['main'];
												setHotelImages(url);
												hotelImages.push(url);
											}
									    }
									}
								);
}


$(document).on('click', '.close', function(e){

	e.preventDefault();
	var url = $(this).attr('url');
	
	removeImage(url);
	$(this).parent('.thumbnail').parent('div').remove();
});

$(document).on('submit', '#idFormAddRoom', function(e){

	e.preventDefault();

	if($(this).valid()){

		var roomType = $('#idRoomType :selected').val();
		var roomLabel = $('#idRoomLabel :selected').val();
		var breakFast = $('#idBreakfast :selected').val();
		var freeCancellation = $('input[name="free_cancellation"]:checked').length == 1 ? 'Y' : 'N';
		var combineOccupant = $('input[name="combine_occupant"]:checked').length == 1 ? 'Y' : 'N';
		var payLater = $('input[name="pay_later"]:checked').length == 1 ? 'Y' : 'N';

		var showDailyPrice = $('input[name="daily_price"]:checked').length == 1 ? 'Y' : 'N';
		var enable = $('input[name="enable"]:checked').length == 1 ? 'Y' : 'N';
		var roomName = $('input[name="room_name"]').val();
		var totalNoRooms = $('input[name="no_of_rooms"]').val();
		var weekdayPrice = parseFloat($('input[name="weekday_price"]').val());
		var weekendPrice = parseFloat($('input[name="weekend_price"]').val());

		var adults = parseInt($('input[name="adult"]').val());
		var childrens = parseInt($('input[name="childrens"]').val());
		var roomSize = $('input[name="room_size"]').val();
		var bedType = $('input[name="bed_type"]').val();
		var roomDescription = $('#idTextRoomDescription').val();
		var facilities = [];

		if(empty(totalNoRooms)){

			showAlertMessage('error', 'Error', 'Total number of rooms can not be blank');
			return false;
		}

		if(empty(roomType)){

			showAlertMessage('error', 'Error', 'Please select room type');
			return false;
		}

		if(empty(adults)){

			showAlertMessage('error', 'Error', 'Number of adults can not be empty');
			return false;
		}

		if(empty(roomLabel)){

			showAlertMessage('error', 'Error', 'Please select room label');
			return false;
		}


		$('.classFacilities').each(function(index, element){

			if($(this).prop('checked') == true){

				facilities.push($(this).attr('name'));
			}
		});


		var arrRoomNumbers = [];
		var objRoomNumbers = {};
		if(!empty(totalNoRooms)){

			$('.classRoomNumber').each(function(index, element){

				
				objRoomNumbers = {

					seq: $(this).attr('data-seq'),
					room_no: $(this).val()
				}

				arrRoomNumbers.push(objRoomNumbers);
			})
		}

		if(arrRoomNumbers.length != totalNoRooms){

			showAlertMessage('error', 'Error', 'Total number of rooms and room number provided do not match');
			return false;	
		}

		var strFacilities = JSON.stringify(facilities);
		var strHotelImages = JSON.stringify(hotelImages);
		var strRoomNumbers = JSON.stringify(arrRoomNumbers);
		
		var formData = {

			room_type: roomType,
			breakfast: breakFast,
			room_label: roomLabel,
			free_cancellation: freeCancellation,
			room_name: roomName,
			combine_occupant: combineOccupant,
			room_description: roomDescription,
			paylater: payLater,
			total_no_rooms: totalNoRooms,
			weekday_price: weekdayPrice,
			weekend_price: weekendPrice,
			adults: adults, 
			childrens: childrens, 
			room_size: roomSize,
			bed_type: bedType,
			show_daily_price: showDailyPrice,
			enable: enable,
			str_hotel_images: strHotelImages,
			str_facilities: strFacilities
		};

		formData = JSON.stringify(formData);
		blockUI();
		$.post('/admin/services/processRequest',
			{
				cmd: 'addRoom',
				data: formData,
				room_seq: _roomSeq,
				room_numbers: strRoomNumbers

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				var retData = $.parseJSON(data['data']);

				if(errorCode == 0 && retData['ret']){
					
					showAlertMessage('success', 'Success', retData['message']);
					setTimeout(function(){ window.top.location = '/admin/rooms' }, 4000);

				}else if(errorCode){

					showAlertMessage('error', 'Error', data['errorMsg']);
				}

				unblockUI();
			}

		)
	}
	
});


function getEditInterface(roomSeq){

	if(!empty(roomSeq)){

		blockUI();
		$.post('/admin/services/processRequest',
			{
				cmd: 'getRoomInfo',
				seq: roomSeq,
				hotel_seq: hotelSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				var row = data['data'][0];
				
				if(!empty(row)){

					$('#idRoomType').val(row['room_type_seq']);
					$('#idRoomLabel').val(row['room_label']);
					$('#idBreakfast').val(row['is_breakfast']);

					if(row['free_cancellation'] == 'Y'){
						
						$('input[name="free_cancellation"]').prop('checked', true);
						$('input[name="free_cancellation"]').parent('span').addClass('checked');
					}

					if(row['combine_occupant'] == 'Y'){
						
						$('input[name="combine_occupant"]').prop('checked', true);
						$('input[name="combine_occupant"]').parent('span').addClass('checked');
					}

					if(row['pay_later'] == 'Y'){
						
						$('input[name="pay_later"]').prop('checked', true);
						$('input[name="pay_later"]').parent('span').addClass('checked');
					}

					if(row['show_daily_price'] == 'Y'){
						
						$('input[name="daily_price"]').prop('checked', true);
						$('input[name="daily_price"]').parent('span').addClass('checked');
					}

					if(row['is_enable'] == 'Y'){
						
						$('input[name="daily_price"]').prop('checked', true);
						$('input[name="enable"]').parent('span').addClass('checked');
					}

					var arrFacilities = $.parseJSON(row['facilities']);
					if(!empty(arrFacilities)){

						$.each(arrFacilities, function(key, val){

							$('input[name="'+val+'"]').prop('checked', true);
							$('input[name="'+val+'"]').parent('span').addClass('checked');
						});
					}

					var arrHotelImages = $.parseJSON(row['images']);
					if(!empty(arrHotelImages)){

						$.each(arrHotelImages, function(key, val){

							setHotelImages(val);
							hotelImages.push(val);
						});
					}

					$('input[name="room_name"]').val(row['room_name']);

					var totalRooms = row['no_of_rooms'];
					$('input[name="no_of_rooms"]').val(totalRooms);

					$('input[name="no_of_rooms"]').change();
					var arrRoomNumbers = data['data']['room_no_info'];

					
					//set room number in text boxes
					if(!empty(arrRoomNumbers)){

						$.each(arrRoomNumbers, function(key, value){

							$('input[name="room_number_'+key+'"]').val(value['room_no']);
							$('input[name="room_number_'+key+'"]').attr('data-seq', value['room_no_seq']);
						});
					}

					$('input[name="weekday_price"]').val(parseFloat(row['weekday_price']));
					$('input[name="weekend_price"]').val(parseFloat(row['weekend_price']));

					$('input[name="adult"]').val(parseInt(row['number_of_adults']));
					$('input[name="childrens"]').val(parseInt(row['number_of_childs']));
					$('input[name="room_size"]').val(row['room_size']);
					$('input[name="bed_type"]').val(row['bed_type']);
					$('#idTextRoomDescription').val(row['room_desc']);
				}

				unblockUI();
			}
		)
	}
}


function setHotelImages(url){

	var strHtml = 
	'<div class="classThumbanil">\
        <div class="thumbnail">\
            <a class="close" href="#" url="'+url+'">×</a>\
            <img src="'+url+'">\
        </div>\
    </div>';

    $('#idDivHotelImages').append(strHtml);
}


function removeImage(url){

	// if(!empty(url)){

	// 	$.post('/admin/services/processRequest',
	// 		{
	// 			cmd: 'removeImage',
	// 			url: url

	// 		},function(response){

				
	// 		}
	// 	)	
	// }

	if(!empty(url)){

		var index = jQuery.inArray(url, hotelImages);
		if(index > -1){

			hotelImages.splice(index, 1);
		}
	}
}

$('#accordion-control-right-group2').on('hide.bs.collapse', function () {

 	$('.panel-heading .panel-title a').html('MORE');
});

$('#accordion-control-right-group2').on('show.bs.collapse', function () {
	
 	$('.panel-heading .panel-title a').html('LESS');
})

$(document).on('change', 'input[name="no_of_rooms"]', function(e){

	var numberOfRooms = parseInt($(this).val());
	var strHtml = '';

	if(!empty(numberOfRooms)){

		strHtml = 	'<div class="row">';
		var i = 0;
		for(var x = 1; x <= numberOfRooms; ++x){

			strHtml += 	'<div class="col-md-2">\
                            <div class="form-group">\
                                <label class="control-label">Room Number '+x+' <span class="colorRed">*<span></label>\
                                <div>\
                                    <input type="number" data-seq="'+x+'" class="form-control classRoomNumber" name="room_number_'+i+'" required>\
                                </div>\
                            </div>\
                        </div>';
            ++i;
            if((x % 6 == 0) && (numberOfRooms > 6)){

            	strHtml += '</div><div class="row">';
            }
		}

		strHtml += '</div>';
	}

	$('#accordion-control-right-group3 .panel-body').html(strHtml);
	$('#accordion-control-right-1').removeClass('hidden');
})