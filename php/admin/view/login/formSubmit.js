
$(document).on('submit', '#formLoginUser', function(e){

    e.preventDefault();

    var hotelId = 1;
    var email = $('input[name="username"]').val(); 
    var password = $('input[name="password"]').val(); 

    if(empty(hotelId)){

        showAlertMessage('error', 'Error', 'Please select any school');
        return false;
    }

    if(empty(email)){

        showAlertMessage('error', 'Error', 'Please enter user name');
        return false;
    }

    if(empty(password)){

        showAlertMessage('error', 'Error', 'Please enter password');
        return false;
    }

    $.post('/admnin/services/authenticate', 
    {
        cmd: "login", 
        hotel_id: hotelId,
        email: email,
        passwd: password 
    },
    function(response){       

        var data = $.parseJSON(response);
        if(data['errorCode'] == 0){

            window.top.location = "/admin/dashboard";
        }else{

            //alert(data['errorMsg']);
            showAlertMessage('error', 'Error', data['errorMsg']);
            return false;
        }
    });
    
});