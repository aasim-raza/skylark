

$(document).ready(function(){

	$('#idModalChangePassword').modal();
});




$(document).on('click', '#idButtonUpdatePassword', function(e){

	e.preventDefault();

	var oldPassword = $('input[name="old_password"]').val();
	var newPassword = $('input[name="new_password"]').val();
	var confirmPassword = $('input[name="password_confirm"]').val();

	if(empty(oldPassword)){

		showAlertMessage('error', 'Error', 'Please enter old password');
		return false;
	}

	if(empty(newPassword)){

		showAlertMessage('error', 'Error', 'Please enter new password');
		return false;
	}

	if(empty(confirmPassword)){

		showAlertMessage('error', 'Error', 'Please enter confirm password');
		return false;
	}


	if(newPassword != confirmPassword){

		showAlertMessage('error', 'Error', 'New password and confirm password are not same');
		return false;	
	}

	
	//blockUI();
	$.post('/admin/services/processRequest',
		{
			cmd: 'updatePassword',
			old_password: oldPassword,
			new_password: newPassword,
			confirm_password: confirmPassword
			
		},function(response){

			var data = $.parseJSON(response);
			var errorCode = data['errorCode'];
			
			if(errorCode == 0 ){
				
				$('#idModalChangePassword').modal('hide');
				showAlertMessage('success', 'Success', 'Password updated successfully');
				setTimeout(function(){ window.top.location = '/admin/dashboard' }, 4000);

			}else if(errorCode){

				showAlertMessage('error', 'Error', data['errorMsg']);
			}

			unblockUI();
		}

	)

});

$(document).on('submit', '#idFormAddRoom', function(e){

	e.preventDefault();

	if($(this).valid()){

		var roomType = $('#idRoomType :selected').val();
		var roomLabel = $('#idRoomLabel :selected').val();
		var breakFast = $('#idBreakfast :selected').val();
		var freeCancellation = $('input[name="free_cancellation"]:checked').length == 1 ? 'Y' : 'N';
		var combineOccupant = $('input[name="combine_occupant"]:checked').length == 1 ? 'Y' : 'N';
		var payLater = $('input[name="pay_later"]:checked').length == 1 ? 'Y' : 'N';
		var showDailyPrice = $('input[name="daily_price"]:checked').length == 1 ? 'Y' : 'N';
		var enable = $('input[name="enable"]:checked').length == 1 ? 'Y' : 'N';
		
		var roomName = $('input[name="room_name"]').val();
		var totalNoRooms = $('input[name="no_of_rooms"]').val();
		var weekdayPrice = parseFloat($('input[name="weekday_price"]').val());
		var weekendPrice = parseFloat($('input[name="weekend_price"]').val());
		var roomSize = $('input[name="room_size"]').val();
		var bedType = $('input[name="bed_type"]').val();
		var roomDescription = $('#idTextRoomDescription').val();
		var facilities = [];

		if(empty(totalNoRooms)){

			showAlertMessage('error', 'Error', 'Total number of rooms can not be blank');
			return false;
		}

		$('.classFacilities').each(function(index, element){

			if($(this).prop('checked') == true){

				facilities.push($(this).attr('name'));
			}
		});

		var strFacilities = JSON.stringify(facilities);
		var strHotelImages = JSON.stringify(hotelImages);

		
		var formData = {

			room_type: roomType,
			breakfast: breakFast,
			room_label: roomLabel,
			free_cancellation: freeCancellation,
			room_name: roomName,
			combine_occupant: combineOccupant,
			room_description: roomDescription,
			paylater: payLater,
			total_no_rooms: totalNoRooms,
			weekday_price: weekdayPrice,
			weekend_price: weekendPrice,
			room_size: roomSize,
			bed_type: bedType,
			show_daily_price: showDailyPrice,
			enable: enable,
			str_hotel_images: strHotelImages,
			str_facilities: strFacilities
		};

		formData = JSON.stringify(formData);
		blockUI();
		$.post('/admin/services/processRequest',
			{
				cmd: 'addRoom',
				data: formData,
				room_seq: _roomSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				var retData = $.parseJSON(data['data']);

				if(errorCode == 0 && retData['ret']){
					
					showAlertMessage('success', 'Success', retData['message']);
					setTimeout(function(){ window.top.location = '/admin/rooms' }, 4000);

				}else if(errorCode){

					showAlertMessage('error', 'Error', data['errorMsg']);
				}

				unblockUI();
			}

		)
	}
	
});


function getEditInterface(roomSeq){

	if(!empty(roomSeq)){

		blockUI();
		$.post('/admin/services/processRequest',
			{
				cmd: 'getRoomInfo',
				seq: roomSeq,
				hotel_seq: hotelSeq

			},function(response){

				var data = $.parseJSON(response);
				var errorCode = data['errorCode'];
				var row = data['data'][0];
				
				if(!empty(row)){

					$('#idRoomType').val(row['room_type_seq']);
					$('#idRoomLabel').val(row['room_label']);
					$('#idBreakfast').val(row['is_breakfast']);

					if(row['free_cancellation'] == 'Y'){
						
						$('input[name="free_cancellation"]').prop('checked', true);
					}

					if(row['combine_occupant'] == 'Y'){
						
						$('input[name="combine_occupant"]').prop('checked', true);
					}

					if(row['pay_later'] == 'Y'){
						
						$('input[name="pay_later"]').prop('checked', true);
					}

					if(row['show_daily_price'] == 'Y'){
						
						$('input[name="daily_price"]').prop('checked', true);
					}

					var arrFacilities = $.parseJSON(row['facilities']);
					if(!empty(arrFacilities)){

						$.each(arrFacilities, function(key, val){

							$('input[name="'+val+'"]').prop('checked', true);
						});
					}

					var arrHotelImages = $.parseJSON(row['images']);
					if(!empty(arrHotelImages)){

						$.each(arrHotelImages, function(key, val){

							setHotelImages(val);
							hotelImages.push(val);
						});
					}

					$('input[name="room_name"]').val(row['room_name']);
					$('input[name="no_of_rooms"]').val(row['no_of_rooms']);
					$('input[name="weekday_price"]').val(parseFloat(row['weekday_price']));
					$('input[name="weekend_price"]').val(parseFloat(row['weekend_price']));
					$('input[name="room_size"]').val(row['room_size']);
					$('input[name="bed_type"]').val(row['bed_type']);
					$('#idTextRoomDescription').val(row['room_desc']);
				}

				unblockUI();
			}
		)
	}
}


function setHotelImages(url){

	var strHtml = 
	'<div class="classThumbanil">\
        <div class="thumbnail">\
            <a class="close" href="#" url="'+url+'">×</a>\
            <img src="'+url+'">\
        </div>\
    </div>';

    $('#idDivHotelImages').append(strHtml);
}


function removeImage(url){

	// if(!empty(url)){

	// 	$.post('/admin/services/processRequest',
	// 		{
	// 			cmd: 'removeImage',
	// 			url: url

	// 		},function(response){

				
	// 		}
	// 	)	
	// }

	if(!empty(url)){

		var index = jQuery.inArray(url, hotelImages);
		if(index > -1){

			hotelImages.splice(index, 1);
		}
	}
}